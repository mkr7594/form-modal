 var modal = document.querySelector('.modal');
var close = document.querySelector('.close');
var table = document.querySelector('#table');
var headerRow, headerFlag = true;
var empid, fname, lname, desig, phone, parent;
var data, infoArray = [];

function showModal() {
    modal.style.display = 'flex';
}

function closeModal() {
    clearFields();
    document.getElementById('b1').style.display = 'block';
    document.getElementById('b2').setAttribute('hidden', true);
    modal.style.display = 'none';
}

function validate() {
    empid = document.querySelector('.one');
    fname = document.querySelector('.two');
    lname = document.querySelector('.three');
    desig = document.querySelector('.four');
    phone = document.querySelector('.five');
    document.getElementById('b1').style.display = 'block';
    
    if(empid.value && fname.value && lname.value && desig.value && phone.value){
        data = {Empid:empid.value, Fname:fname.value, Lname:lname.value, Designation:desig.value, Phone:phone.value};
        infoArray.push(data);
        closeModal();
        createTable(data,headerFlag);
        clearFields();
    }else {
        alert('Fields are blank!!Please fill the missing details');
    }
}

function createTable(data,headerRow) {
    table.removeAttribute('hidden');
    if (headerFlag) {
        headerRow = document.createElement('tr');
        headerRow.setAttribute('id','header')
        headerRow.innerHTML = Object.keys(data).map((cur) => `<td><b>${cur}</b><td>`).join('');
        headerRow.innerHTML += `<td colspan='2' align='center'><b>Action</b></td>`
        table.appendChild(headerRow);
        headerFlag = false;
    }
    var row = document.createElement('tr');
    row.innerHTML = Object.values(data).map((cur)=>`<td>${cur}<td>`).join('');

    var editData = document.createElement('td');
    var edit = document.createElement('button');
    edit.setAttribute('class','button');
    edit.setAttribute('id', 'edit');
    edit.setAttribute('onclick', 'showEdit(event)');
    edit.innerHTML = 'EDIT';
    editData.appendChild(edit);
    row.appendChild(editData);

    var deleteData = document.createElement('td');
    var del = document.createElement('button');
    del.setAttribute('class','button');
    del.setAttribute('onclick', 'showDelete(event)');
    del.setAttribute('id', 'delete');
    del.innerHTML = 'DELETE';
    deleteData.appendChild(del);
    row.appendChild(deleteData);
    table.appendChild(row);
}

function clearFields() {
    empid.value = '';
    fname.value = '';
    lname.value = '';
    desig.value = '';
    phone.value = '';
}

function showEdit(e) {
    modal.style.display = 'flex';
    document.getElementById('b1').style.display = 'none';
    document.getElementById('b2').removeAttribute('hidden');
    parent = e.target.parentElement.parentElement;
    var childs = parent.childNodes;
    var childArr = Array.from(childs);
    var filArr = childArr.filter((item)=>{
        if(item.innerHTML !== '' && item.innerText !== 'EDIT' && item.innerText !== 'DELETE') {
            return item.innerHTML;
        }
    });
    var newValues = filArr.map((item)=>{
        return item.innerHTML;
    });
    
    empid.value = newValues[0];
    fname.value = newValues[1];
    lname.value = newValues[2];
    desig.value = newValues[3];
    phone.value = newValues[4];
}

function editData() {
    document.getElementById('b1').style.display = 'block';
    document.getElementById('b2').setAttribute('hidden', true);
    var childs = parent.childNodes;
    var childArr = Array.from(childs);
    var filArr = childArr.filter((item) => {
        if (item.innerHTML !== '' && item.innerText !== 'EDIT' && item.innerText !== 'DELETE') {
            return item;
        }
    });
    filArr[0].innerHTML = empid.value;
    filArr[1].innerHTML = fname.value;
    filArr[2].innerHTML = lname.value;
    filArr[3].innerHTML = desig.value;
    filArr[4].innerHTML = phone.value;
    clearFields();
    modal.style.display = 'none';
}

function showDelete(e) {
    var parent = e.target.parentElement.parentElement;
    document.getElementById('table').removeChild(parent);
}